package com.certox;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerPreLoginEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * User: 200dvd
 * Date: 14.08.13
 * Time: 19:54
 */
public class Core extends JavaPlugin implements Listener {

    public MySQL db = new MySQL();
    Core plugin = this;

    Boolean forceMode = false;
    String forceModeMsg = "";

    Boolean serverListPing = false;
    String serverListPingMsg = "";

    String kickMsg = "";

    boolean enabled = true;
    boolean debug = false;

    private Map<String, Object> activeBlacklist = new HashMap<String, Object>();

    @Override
    public void onLoad() {

        // DATABASE
        getConfig().addDefault("AntiJoinBot.MySQL.Offline",true);
        getConfig().addDefault("AntiJoinBot.MySQL.Host",  "localhost");
        getConfig().addDefault("AntiJoinBot.MySQL.Port",  3306);
        getConfig().addDefault("AntiJoinBot.MySQL.Database",  "ajb_intelligent_blacklist");
        getConfig().addDefault("AntiJoinBot.MySQL.User",  "<username>");
        getConfig().addDefault("AntiJoinBot.MySQL.Password",  "<password>");

        // BLACKLISTS
        activeBlacklist.put("http://www,shroomery,org/ythan/proxycheck,php?ip=","Y");
        activeBlacklist.put("http://www,stopforumspam,com/api?ip=","yes");
        activeBlacklist.put("http://yasb,intuxication,org/api/check,xml?ip=","<spam>true</spam>");

        getConfig().addDefault("AntiJoinBot.Blacklists", activeBlacklist);

        // MISC
        getConfig().addDefault("AntiJoinBot.ServerListPing.Enabled",  false);
        getConfig().addDefault("AntiJoinBot.ServerListPing.Message",  "Don't use a Proxy :P");

        getConfig().addDefault("AntiJoinBot.Force.Enabled",  false);
        getConfig().addDefault("AntiJoinBot.Force.Message",  "Proxy Check... pls. reJoin!");

        getConfig().addDefault("AntiJoinBot.Kick.Message",  "Proxy is Detected. (maybe an error please reconnect your Router)");

        getConfig().addDefault("AntiJoinBot.Warmup.Enabled",  true);
        getConfig().addDefault("AntiJoinBot.Warmup.Seconds",  60);

        getConfig().addDefault("AntiJoinBot.Debug.Enabled",  false);

        getConfig().options().copyDefaults(true);
        saveConfig();
        reloadConfig();

        debug = getConfig().getBoolean("AntiJoinBot.Debug.Enabled");

        serverListPing = getConfig().getBoolean("AntiJoinBot.ServerListPing.Enabled");
        serverListPingMsg = getConfig().getString("AntiJoinBot.ServerListPing.Message");

        forceMode = getConfig().getBoolean("AntiJoinBot.Force.Enabled");
        forceModeMsg = getConfig().getString("AntiJoinBot.Force.Message");

        kickMsg = getConfig().getString("AntiJoinBot.Kick.Message");

        activeBlacklist = getConfig().getConfigurationSection("AntiJoinBot.Blacklists").getValues(false);
    }

    @Override
    public void onEnable() {

        if(getConfig().getBoolean("AntiJoinBot.Warmup.Enabled")){
        System.out.println("AntiJoinBot enabled in "+ getConfig().getInt("AntiJoinBot.Warmup.Seconds") +" seconds");
        getServer().getScheduler().runTaskLaterAsynchronously(this, new Runnable() {
            public void run() {
                System.out.println("AntiJoinBot is enabled");
                Bukkit.getPluginManager().registerEvents(plugin, plugin);
            }

        }, getConfig().getInt("AntiJoinBot.Warmup.Seconds")*20);
        }
        else
        Bukkit.getPluginManager().registerEvents(plugin, plugin);

        try {
            db.connect(
                    getConfig().getString("AntiJoinBot.MySQL.Host"),
                    getConfig().getInt("AntiJoinBot.MySQL.Port"),
                    getConfig().getString("AntiJoinBot.MySQL.Database"),
                    getConfig().getString("AntiJoinBot.MySQL.User"),
                    getConfig().getString("AntiJoinBot.MySQL.Password"),
                    getConfig().getBoolean("AntiJoinBot.MySQL.Offline"));
            db.initDB();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println(">>>>>>>>>>>> MySQL DRIVER NOT FOUND <<<<<<<<<<<<");
            e.printStackTrace();
        }
        try {
            db.loadDBtoRAM();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean isProxy(String IP){
        if(IP.equals("127.0.0.1") || IP.equals("localhost"))
            return false;
        for(String s : activeBlacklist.keySet()){
            try {
            String res = "";
            Scanner scanner = new Scanner(new URL(s.replace(",",".") + IP).openStream());
            while(scanner.hasNextLine()){
                res = res + scanner.nextLine();
            }
            String[] args = ((String)activeBlacklist.get(s)).split(",");
            for(String arg : args)
                if(res.contains(arg)){
                    debug(s.replace(",",".") +": ("+ IP + " --> true)");
                    return true;
                }
                else
                    debug(s.replace(",",".") +": ("+ IP + " --> false)");
            } catch (Exception e) {
                if(debug)
                e.printStackTrace();
            }
        }

        return false;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent e) throws Exception  {
        if(!enabled)
            return;

        if(e.getPlayer().hasPermission("ajb.bypass")){
            db.setUser(e.getPlayer(),false);
            return;
        }

    }
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onServerListPingEvent(ServerListPingEvent e) throws Exception  {
        if(!serverListPing)
            return;
        if(db.ipBlacklist.containsKey(e.getAddress().getHostAddress().toString())){
            debug("[M] ipBlacklist: "+ e.getAddress().getHostAddress().toString() + " --> " + db.ipBlacklist.get(e.getAddress().getHostAddress().toString()));
            if(db.ipBlacklist.get(e.getAddress().getHostAddress().toString())){
                e.setMotd(serverListPingMsg);
            }
            return;
        }
        final String IP = e.getAddress().getHostAddress().toString();
        getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            public void run() {
                try {
                    plugin.db.setIP(IP,isProxy(IP));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLoginEvent(PlayerLoginEvent e) throws Exception  {
        if(!enabled)
            return;
        debug("[M] JOIN: "+ e.getAddress().getHostAddress().toString() + " --> " + e.getPlayer().getName());
        if(db.userBlacklist.containsKey(e.getPlayer().getName()))  {
            debug("[M] userBlacklist: "+ e.getPlayer().getName() + " --> " + db.userBlacklist.get(e.getPlayer().getName()));
            if(db.userBlacklist.get(e.getPlayer().getName())){
                e.setKickMessage(kickMsg);
                e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            }
            return;
        }
        if(db.ipBlacklist.containsKey(e.getAddress().getHostAddress().toString())){
            debug("[M] ipBlacklist: "+ e.getAddress().getHostAddress().toString() + " --> " + db.ipBlacklist.get(e.getAddress().getHostAddress().toString()));
            if(db.ipBlacklist.get(e.getAddress().getHostAddress().toString())){
                e.setKickMessage(kickMsg);
                e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            }
            return;
        }


        if(e.getPlayer().hasPermission("ajb.bypass")){
            db.setUser(e.getPlayer(),false);
            return;
        }

        if(forceMode){
            debug("[M] FORCE: "+ e.getAddress().getHostAddress().toString() + " --> " + e.getPlayer().getName());
            final String IP = e.getAddress().getHostAddress().toString();
            getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
                public void run() {
                    try {
                        plugin.db.setIP(IP,isProxy(IP));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
            e.setKickMessage(forceModeMsg);
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            return;
        }
        if(isProxy(e.getAddress().getHostAddress().toString())){
            db.setIP(e.getAddress().getHostAddress().toString(),true);
            e.setKickMessage(kickMsg);
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
        }
        else
        db.setIP(e.getAddress().getHostAddress().toString(), false);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLoginEvent(AsyncPlayerPreLoginEvent e) throws Exception  {
        if(!enabled || forceMode)
            return;

        debug("[A] JOIN: "+ e.getAddress().getHostAddress().toString() + " --> " + e.getName());
        if(db.userBlacklist.containsKey(e.getName())){
            debug("[A] userBlacklist: "+ e.getName() + " --> " + db.userBlacklist.get(e.getName()));
            if(db.userBlacklist.get(e.getName())){
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, kickMsg);
            }
            return;
        }
        if(db.ipBlacklist.containsKey(e.getAddress().getHostAddress().toString())){
            debug("[A] ipBlacklist: "+ e.getAddress().getHostAddress().toString() + " --> " + db.ipBlacklist.get(e.getAddress().getHostAddress().toString()));
            if(db.ipBlacklist.get(e.getAddress().getHostAddress().toString())){
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, kickMsg);
            }
            return;
        }

        if(isProxy(e.getAddress().getHostAddress().toString())){
            db.setIP(e.getAddress().getHostAddress().toString(),true);
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, kickMsg);
        }
        else
            db.setIP(e.getAddress().getHostAddress().toString(), false);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(sender.hasPermission("ajb.toggle"))
            if(cmd.getName().equals("ajb") && args.length == 0){
                if(enabled)
                    enabled = false;
                else
                    enabled = true;

                sender.sendMessage("AntiJoinBot Enabled: " + enabled);
            }

        if(sender.hasPermission("ajb.add"))
            try {
                if(db.isConnected())
                    if(cmd.getName().equals("ajb") && args.length == 2)
                        switch (args[0]) {
                            case "add":
                                db.setUser(args[1], false);
                                sender.sendMessage("AntiJoinBot Player add to whitelist: " + args[1]);
                                break;
                            case "block":
                                sender.sendMessage("AntiJoinBot Player add to blacklist: " + args[1]);
                                db.setUser(args[1], true);
                                break;
                        }
            } catch (SQLException e) {
            }
        return false;
    }

    public void debug(String text){
        if(debug)
            System.out.println("AJB: [D] "+ text);
    }
}
